#include <stdio.h>
#include <stdlib.h>
#include <string.h>



//declaring struct objects

typedef struct Process {
   	int pid;
	int tempsArrive;
   	int priorite;
	int execTemps;
	int nombreImprimantes;
	int nombreScanneurs;
	int nombreModems;
	int nombreCds;
	struct Process* next;
}Process;

struct Process * createListFromFile(char * path){

	typedef char * string;

	int ints[7];  // Make an array of 7 integers --> will be used when tokenizing the input lines
	struct Process *processes;
	processes = (struct Process*)malloc(sizeof(*processes));

  	int i;

	int counter = 0;
	int intsIndex = 0;
	FILE * fp;
       	char * line = NULL;
       	size_t len = 0;
       	ssize_t read;

       	fp = fopen(path, "r");
       	if (fp == NULL)
           	exit(EXIT_FAILURE);

	char * pch;
	struct Process *tempWriteProcess  = processes;


	while ((read = getline(&line, &len, fp)) != -1) {
		pch = strtok (line,", ");
		intsIndex = 0;
		while (pch != NULL)
		{
			ints[intsIndex] = atoi(pch); //casting char pointer to integer pointer in order to avoid WARNING
			//printf("\n%s", pch);
			pch = strtok (NULL, ", ");
			intsIndex++; //increment index that loops through input line --> goes by ", " delimiter
		}
		//pass values into Struct Processes
		tempWriteProcess->tempsArrive = ints[0];
		tempWriteProcess->priorite = ints[1];
		tempWriteProcess->execTemps = ints[2];
		tempWriteProcess->nombreImprimantes = ints[3];
		tempWriteProcess->nombreScanneurs = ints[4];
		tempWriteProcess->nombreModems = ints[5];
		tempWriteProcess->nombreCds = ints[6];
		//printf("\nAdded all members to tempProcess\n");
		struct Process *nextProcess;
		nextProcess = (struct Process*)malloc(sizeof(*nextProcess));

		tempWriteProcess->next = nextProcess;

		tempWriteProcess = nextProcess;
		tempWriteProcess->next=NULL;

		//increment counter for processes --> lines read from input file
		counter++;
        }
	/*
	//test --> read values from Struct Processes
	struct Process *tempReadProcess  = processes;
	for(i = 0;i < 3;++i)
	{
		printf("\n\nProcessus %i\n", i);
		printf("temps darrivee %i\n", tempReadProcess->tempsArrive);
		printf("priorite %i\n", tempReadProcess->priorite);
		printf("exec time %i\n", tempReadProcess->execTemps);
		printf("nbr imprim  %i\n", tempReadProcess->nombreImprimantes);
		printf("nbr scannr %i\n", tempReadProcess->nombreScanneurs);
		printf("nbr modems %i\n", tempReadProcess->nombreModems);
		printf("nbr cds %i\n", tempReadProcess->nombreCds);
		tempReadProcess = tempReadProcess->next;
	}
	*/
	fclose(fp);
	return processes;

}

int main(int argc, char *argv[]){
	if(argc==2){
		struct Process *ProcessList;
		ProcessList=createListFromFile(argv[1]);
		printf("temps darrivee %i\n", ProcessList->next->next->tempsArrive);

		//define list that will contain real-time processes
		struct Process *RealTimeProcesses;
		RealTimeProcesses = (struct Process*)malloc(sizeof(*RealTimeProcesses));
		struct Process *CurrentRealTimeProcesses;
		CurrentRealTimeProcesses = (struct Process*)malloc(sizeof(*CurrentRealTimeProcesses));

		//define list that will contain user-processes
		struct Process *UserPriorityProcesses;
		UserPriorityProcesses = (struct Process*)malloc(sizeof(*UserPriorityProcesses));
		struct Process *CurrentUserPriorityProcesses;
		CurrentUserPriorityProcesses = (struct Process*)malloc(sizeof(*CurrentUserPriorityProcesses));

		struct Process *CurrentProcess;


		//inserting jobs into appropriate queues
		CurrentProcess = ProcessList;
		while (CurrentProcess->next != NULL)
		{
			printf("temps darrivee %i\n", CurrentProcess->tempsArrive);
			printf("priorite %i\n", CurrentProcess->priorite);

			switch (CurrentProcess->priorite) {
			case 0:
				printf("here process 0");
				if (RealTimeProcesses == NULL) //first process added to this queue
				{
					RealTimeProcesses = CurrentProcess;
					CurrentRealTimeProcesses = RealTimeProcesses;
					printf("just added first node into ProcessPriority0\n");
				}
				else
				{
					CurrentRealTimeProcesses->next = CurrentProcess;
					CurrentRealTimeProcesses = CurrentRealTimeProcesses->next;
					printf("just added 2nd, 3rd ... node into RealTimeProcesses\n");
				}
				break;
			default: //used for priorities 1,2,3
				printf("here");
				if (UserPriorityProcesses == NULL) //first process added to this queue
				{
					UserPriorityProcesses = CurrentProcess;
					CurrentUserPriorityProcesses = UserPriorityProcesses;
					printf("just added first node into UserPriorityProcesses\n");
				}
				else //all remaining processes will be added to the queue here
				{
					CurrentUserPriorityProcesses->next = CurrentProcess;
					CurrentUserPriorityProcesses = CurrentUserPriorityProcesses->next;
					printf("just added 2nd, 3rd ... node into UserPriorityProcesses\n");
				}
				break;
            		}

			CurrentProcess = CurrentProcess->next;
		}


		//TEST reading out of the RealTimeProcesses
		CurrentProcess = RealTimeProcesses;
		while (CurrentProcess->next != NULL)
		{
			printf("temps darrivee %i\n", CurrentProcess->tempsArrive);
			CurrentProcess = CurrentProcess->next;
		}
		//TEST 2 --> getting lowestPriorityProcess
		//CurrentProcess = getNextProcess(RealTimeProcesses, UserPriorityProcesses, 10);

	}
	else{
		printf("Il faut appeler ce script de la manière suivante : ./LOG710H14_Lab02 pathFichierTexte");
	}
}

struct Process * getNextProcess(struct Process * RealTimeProcesses, struct Process * UserPriorityProcesses, int currentTime){
	if (RealTimeProcesses != NULL)
	{
		return RealTimeProcesses;
	}
	else if (UserPriorityProcesses != NULL)
	{
		struct Process *CurrentProcess;
		struct Process *LowestPriorityProcess;
		CurrentProcess = UserPriorityProcesses;
		LowestPriorityProcess = UserPriorityProcesses;
		//loop through all UserPriorityProcesses, find the one with the arrivalTime equal or lower that current time
		while (CurrentProcess != NULL)
		{
			if (CurrentProcess->tempsArrive >= currentTime)
			{
				LowestPriorityProcess = CurrentProcess;
				break;
			}
			CurrentProcess = CurrentProcess->next;
		}
		//loop through all UserPriorityProcesses, find the one with the lowest priorite

		CurrentProcess = UserPriorityProcesses;

		while (CurrentProcess != NULL)
		{
			if ((LowestPriorityProcess->priorite > CurrentProcess->priorite) && (CurrentProcess->tempsArrive >= currentTime))
			{
				LowestPriorityProcess = CurrentProcess;
			}
			CurrentProcess = CurrentProcess->next;
		}
		return LowestPriorityProcess;
	}
	else
	{
		return NULL;
	}


}
