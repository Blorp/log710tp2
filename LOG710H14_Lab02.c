#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>



//declaring struct objects

typedef struct Process {
   	int pid;
	int tempsArrive;
   	int priorite;
	int execTemps;
	int nombreImprimantes;
	int nombreScanneurs;
	int nombreModems;
	int nombreCds;
	int processTermine;
	int tempsEcoule;
	struct Process* next;
}Process;

struct Process *ReadyProcess;
//ReadyProcess = (struct Process*)malloc(sizeof(*ReadyProcess));


struct Process * createRealTimeProcessesListFromFile(char * path){

	typedef char * string;

	int ints[7];  // Make an array of 7 integers --> will be used when tokenizing the input lines
	struct Process *processes;
	processes = (struct Process*)malloc(sizeof(*processes));

  	int i;

	int counter = 0;
	int intsIndex = 0;
	FILE * fp;
       	char * line = NULL;
       	size_t len = 0;
       	ssize_t read;

       	fp = fopen(path, "r");
       	if (fp == NULL)
           	exit(EXIT_FAILURE);

	char * pch;
	struct Process *tempWriteProcess  = processes;
	

	while ((read = getline(&line, &len, fp)) != -1) {
		pch = strtok (line,", ");
		intsIndex = 0;
		while (pch != NULL)
		{
			ints[intsIndex] = atoi(pch); //casting char pointer to integer pointer in order to avoid WARNING
			pch = strtok (NULL, ", ");
			intsIndex++; //increment index that loops through input line --> goes by ", " delimiter 
		} 
		//pass values into Struct Processes
		if (ints[1] == 0)
		{
			tempWriteProcess->tempsArrive = ints[0];
			tempWriteProcess->priorite = ints[1];
			tempWriteProcess->execTemps = ints[2];
			tempWriteProcess->nombreImprimantes = ints[3];
			tempWriteProcess->nombreScanneurs = ints[4];
			tempWriteProcess->nombreModems = ints[5];
			tempWriteProcess->nombreCds = ints[6];
			tempWriteProcess->processTermine = 0;
			tempWriteProcess->tempsEcoule = 0;
			
			struct Process *nextProcess;
			nextProcess = (struct Process*)malloc(sizeof(*nextProcess));
	
			tempWriteProcess->next = nextProcess;
			
			tempWriteProcess = nextProcess;
			tempWriteProcess->next=NULL;
	
			//increment counter for processes --> lines read from input file		
			counter++;
		}
        }

	fclose(fp);
	return processes;
	
}


struct Process * createUserPriorityListFromFile(char * path){

	typedef char * string;

	int ints[7];  // Make an array of 7 integers --> will be used when tokenizing the input lines
	struct Process *processes;
	processes = (struct Process*)malloc(sizeof(*processes));

  	int i;

	int counter = 0;
	int intsIndex = 0;
	FILE * fp;
       	char * line = NULL;
       	size_t len = 0;
       	ssize_t read;

       	fp = fopen(path, "r");
       	if (fp == NULL)
           	exit(EXIT_FAILURE);

	char * pch;
	struct Process *tempWriteProcess  = processes;
	

	while ((read = getline(&line, &len, fp)) != -1) {
		pch = strtok (line,", ");
		intsIndex = 0;
		while (pch != NULL)
		{
			ints[intsIndex] = atoi(pch); //casting char pointer to integer pointer in order to avoid WARNING
			pch = strtok (NULL, ", ");
			intsIndex++; //increment index that loops through input line --> goes by ", " delimiter 
		} 
		//pass values into Struct Processes
		if (ints[1] == 1 || ints[1] == 2 || ints[1] == 3)
		{
			tempWriteProcess->tempsArrive = ints[0];
			tempWriteProcess->priorite = ints[1];
			tempWriteProcess->execTemps = ints[2];
			tempWriteProcess->nombreImprimantes = ints[3];
			tempWriteProcess->nombreScanneurs = ints[4];
			tempWriteProcess->nombreModems = ints[5];
			tempWriteProcess->nombreCds = ints[6];
			tempWriteProcess->processTermine = 0;
			tempWriteProcess->tempsEcoule = 0;
			
			struct Process *nextProcess;
			nextProcess = (struct Process*)malloc(sizeof(*nextProcess));
	
			tempWriteProcess->next = nextProcess;
			
			tempWriteProcess = nextProcess;
			tempWriteProcess->next=NULL;
	
			//increment counter for processes --> lines read from input file		
			counter++;
		}
        }

	fclose(fp);
	return processes;
	
}



//struct Process * getNextProcess(struct Process *RealTimeProcessesInput){
void getNextProcess(struct Process *RealTimeProcessesInput, struct Process *UserPriorityProcessesInput, struct Process *NextReadyProcessInput){
	
	struct Process *CurrentRealTimeProcess;
	struct Process *CurrentUserPriorityProcess;
	struct Process *CurrentUserPriorityProcess2;	
	
	int areListsEmpty = 0;
    	CurrentRealTimeProcess = RealTimeProcessesInput;
	CurrentUserPriorityProcess = UserPriorityProcessesInput;
	CurrentUserPriorityProcess2 = UserPriorityProcessesInput;

	

	areListsEmpty = 0;
	int counter = 0;
		if (RealTimeProcessesInput->next != NULL)
		{
			//get first NOT COMPLETED process from real-time queue
			while(CurrentRealTimeProcess->next != NULL )
			{
				if (CurrentRealTimeProcess->processTermine == 0)
				{
					//printf("Setting FIRST user process\n");
					ReadyProcess = CurrentRealTimeProcess;
					break;
				}
				CurrentRealTimeProcess = CurrentRealTimeProcess->next;
			}

			ReadyProcess = CurrentRealTimeProcess;

		}
		else
		{
			//get first NOT COMPLETED process from user priority queue
			while(CurrentUserPriorityProcess->next != NULL )
			{
				if (CurrentUserPriorityProcess->processTermine == 0)
				{
					ReadyProcess = CurrentUserPriorityProcess;
					break;
				}

				CurrentUserPriorityProcess = CurrentUserPriorityProcess->next;
				counter++;
			}
			while(CurrentUserPriorityProcess->next != NULL)
			{
				if (CurrentUserPriorityProcess->processTermine == 1)
					continue;

				if (ReadyProcess->priorite > CurrentUserPriorityProcess->priorite)
				{
					ReadyProcess = CurrentUserPriorityProcess;
					
				}
				CurrentUserPriorityProcess = CurrentUserPriorityProcess->next;
			}
		}
	//}


}






int main(int argc, char *argv[]){
	if(argc==2){
		struct Process *RealTimeProcesses;
		RealTimeProcesses=createRealTimeProcessesListFromFile(argv[1]);

		struct Process *UserPriorityProcesses;
		UserPriorityProcesses=createUserPriorityListFromFile(argv[1]);

		struct Process *CurrentRealTimeProcess;
		struct Process *CurrentUserPriorityProcess;

		struct Process *NextReadyProcess;
		NextReadyProcess=createUserPriorityListFromFile(argv[1]);

		//starting to create processes
		pid_t pid;
		int counter = 0;
		int current_running_pid = -1;
		while(counter <= 50)
		{
			printf("Conuter = %i\n", counter); 
			getNextProcess(RealTimeProcesses, UserPriorityProcesses, NextReadyProcess);
			if (ReadyProcess != NULL)
			{
				//check if process is completed
				if (ReadyProcess->tempsEcoule >= ReadyProcess->execTemps)
				{
					ReadyProcess->processTermine = 1;
					kill(ReadyProcess->pid, SIGINT);
					getNextProcess(RealTimeProcesses, UserPriorityProcesses, NextReadyProcess);
				}
						printf("temps darrivee %i\n", ReadyProcess->tempsArrive);
						printf("priorite %i\n", ReadyProcess->priorite);
						printf("current_running_pid %i\n", current_running_pid);
				if (ReadyProcess->priorite == 0)
				{
					
					if (ReadyProcess->pid != current_running_pid) //if there is a Priority 0 process ready that is NOT hte one running at this moment --> run it
					{						
						pid=fork();
						if(pid==0)
						{	
							static char *argv[]={"log710h14process",NULL};
						      	execv("/bin/log710h14process",argv);
						}
						else{
							current_running_pid = getpid(pid);
							ReadyProcess->pid = getpid(pid);
							printf("Done!\n");
						}
					}
					else
					{		
						ReadyProcess->tempsEcoule++;
					}
				}
			}

			sleep(1);
			counter++;
		}
	}

	
	else{
		printf("Il faut appeler ce script de la manière suivante : ./LOG710H14_Lab02 pathFichierTexte");
	}
}


